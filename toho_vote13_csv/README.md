# Overview

[第13回 東方プロジェクト 人気投票](http://toho-vote.info)の

- [人妖部門](http://toho-vote.info/result_list_character.php)
- [音楽部門](http://toho-vote.info/result_list_music.php)
- [作品部門](http://toho-vote.info/result_list_title.php)
- [ベストパートナー部門](http://toho-vote.info/result_list_partner.php)

の各データをExcel等で開くことのできるCSV形式に書き出したものになります。

# License

[第13回 東方プロジェクト 人気投票 公式ページ](http://toho-vote.info)より
>※本投票において公開されたデータは、統計や考察などの目的であれば、自己の責任に基づき自由に使用しても構いません（その際は必ず出典を明示してください）。

との記述がありますのでこれに則っていただければ問題ないかと思われます。
